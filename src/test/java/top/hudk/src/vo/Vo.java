package top.hudk.src.vo;

import lombok.Data;

/**
 * 作用:
 *
 * @author hudk
 * @date 2020/1/5 18:27
 */

@Data
public class Vo {

    private String name;

    private String code;

}
