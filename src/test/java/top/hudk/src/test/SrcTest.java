package top.hudk.src.test;

import com.sun.tools.javac.util.GraphUtils;
import org.junit.Test;

import java.util.LinkedList;

/**
 * 作用:
 *
 * @author hudk
 * @date 2020/1/5 18:01
 */
public class SrcTest {

    /**
     * 测试链表的包含方法
     */
    @Test
    public void LinkedListTestContains(){
        LinkedList<String> link = new LinkedList<String>();
        String s1 = new String("abc");
        link.add(s1);
        String s2 = new String("abc");
        System.out.println(link.contains(s2));
        System.out.println(s1.equals(s2));
        System.out.println(s1 == s2);
    }

    /**
     * 测试链表的包含方法
     */
    @Test
    public void LinkedListTestRing(){
        LinkedList<String> link = new LinkedList<String>();
        String s0 = "a";
        String s1 = "b";
        String s2 = "c";
        String s3 = "d";
        String s4 = "e";
        String s5 = "f";
        String s6 = "g";
        String s7 = "h";
        String s8 = "i";
        String s9 = "g";
        link.add(s0);
        link.add(s1);
        link.add(s2);
        link.add(s3);
        link.add(s4);
        link.add(s5);
        link.add(s6);
        link.add(s7);
        link.add(s8);
        link.add(s9);

        System.out.println(link.contains(s2));
        System.out.println(s1.equals(s2));
        System.out.println(s1 == s2);
    }


}
