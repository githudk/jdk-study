import com.sun.xml.internal.fastinfoset.stax.factory.StAXOutputFactory;

/**
 * 作用:主类
 *
 * @author hudk
 * @date 2020/1/5 13:26
 */
public class Main {

    public static void main(String[] args){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n");
        stringBuilder.append("******************************************\n");
        stringBuilder.append("*        这是一个JDK源码学习项目           \n");
        stringBuilder.append("*         JDK版本：1.8.0_221             \n");
        stringBuilder.append("*   项目内容，主要是对源码的注释和思考      \n");
        stringBuilder.append("******************************************\n");
        System.out.println(stringBuilder);
    }
}
